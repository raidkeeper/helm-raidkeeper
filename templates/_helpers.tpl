{{- define "image.application" -}}
{{- $registry := default .Values.global.image.registry .Values.application.image.registry -}}
{{- $image := default .Values.global.image.name .Values.application.image.name -}}
{{- $tag := default (tpl .Values.global.image.tag .) .Values.application.image.tag -}}
{{- printf "%s/%s:%s" $registry $image $tag -}}
{{- end -}}

{{- define "image.nginx" -}}
{{- $registry := default .Values.global.image.registry .Values.nginx.image.registry -}}
{{- $image := default .Values.global.image.name .Values.nginx.image.name -}}
{{- $tag := default (tpl .Values.global.image.tag .) (tpl .Values.nginx.image.tag .) -}}
{{- printf "%s/%s:%s" $registry $image $tag -}}
{{- end -}}

{{- define "image.redis" -}}
{{- $registry := default .Values.global.image.registry .Values.redis.image.registry -}}
{{- $image := default .Values.global.image.name .Values.redis.image.name -}}
{{- $tag := default (tpl .Values.global.image.tag .) .Values.redis.image.tag -}}
{{- printf "%s/%s:%s" $registry $image $tag -}}
{{- end -}}

{{- define "image.scheduler" -}}
{{- $registry := default .Values.global.image.registry .Values.scheduler.image.registry -}}
{{- $image := default .Values.global.image.name .Values.scheduler.image.name -}}
{{- $tag := default (tpl .Values.global.image.tag .) .Values.scheduler.image.tag -}}
{{- printf "%s/%s:%s" $registry $image $tag -}}
{{- end -}}

{{- define "image.worker" -}}
{{- $registry := default .Values.global.image.registry .Values.worker.image.registry -}}
{{- $image := default .Values.global.image.name .Values.worker.image.name -}}
{{- $tag := default (tpl .Values.global.image.tag .) .Values.worker.image.tag -}}
{{- printf "%s/%s:%s" $registry $image $tag -}}
{{- end -}}

{{- define "hostname" -}}
{{- print (first .Values.nginx.hostnames) -}}
{{- end -}}
